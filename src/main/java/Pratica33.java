import utfpr.ct.dainf.if62c.pratica.Matriz;

/**
 * IF62C Fundamentos de Programação 2
 * Exemplo de programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica33 {

    public static void main(String[] args) {
        Matriz orig = new Matriz(3, 2);
        double[][] m = orig.getMatriz();
        m[0][0] = 0.0;
        m[0][1] = 0.1;
        m[1][0] = 1.0;
        m[1][1] = 1.1;
        m[2][0] = 2.0;
        m[2][1] = 2.1;
        Matriz transp = orig.getTransposta();
        System.out.println("Matriz original: " + orig);
        System.out.println("Matriz transposta: " + transp);
        
        Matriz a = new Matriz(3, 2);
        double[][] ma = a.getMatriz();
        ma[0][0] = 0.0;
        ma[0][1] = 1.0;
        ma[1][0] = 0.0;
        ma[1][1] = 2.0;
        ma[2][0] = 0.0;
        ma[2][1] = 3.0;
        Matriz b = new Matriz(3, 2);
        double[][] mb = b.getMatriz();
        mb[0][0] = 1.0;
        mb[0][1] = 2.0;
        mb[1][0] = 3.0;
        mb[1][1] = 4.0;
        mb[2][0] = 5.0;
        mb[2][1] = 6.0;
        Matriz s = a.soma(b);
        System.out.println("Matriz A: " + a);
        System.out.println("Matriz B: " + b);
        System.out.println("Matriz A+B: " + s);
        Matriz c = new Matriz(2, 4);
        double[][] mc = c.getMatriz();
        mc[0][0] = 0.0;
        mc[0][1] = 1.0;
        mc[0][2] = 2.0;
        mc[0][3] = 3.0;
        mc[1][0] = 1.0;
        mc[1][1] = 1.0;
        mc[1][2] = 2.0;
        mc[1][3] = 2.0;
        Matriz p = a.prod(c);
        System.out.println("Matriz A: " + a);
        System.out.println("Matriz C: " + b);
        System.out.println("Matriz AxC: " + p);
    }
}
